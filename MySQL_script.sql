
-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`Guest`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Guest` (
  `Id_guest` INT NOT NULL,
  `Name` VARCHAR(45) NOT NULL,
  `Surname` VARCHAR(45) NOT NULL,
  `Birth` DATE NOT NULL,
  PRIMARY KEY (`Id_guest`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Food`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Food` (
  `id_Food` INT NOT NULL,
  `Food_type` VARCHAR(45) NOT NULL,
  `Food_price` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id_Food`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Guest_has_food`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Guest_has_food` (
  `Id_guest` INT NOT NULL,
  `Id_Food` INT NOT NULL,
  `Total_price` INT NOT NULL,
  PRIMARY KEY (`Id_guest`, `Id_Food`),
  INDEX `fk_Guest_has_food_Food1_idx` (`Id_Food` ASC) VISIBLE,
  INDEX `fk_Guest_has_food_Guest1_idx` (`Id_guest` ASC) VISIBLE,
  CONSTRAINT `fk_Guest_has_food_Food1`
    FOREIGN KEY (`Id_Food`)
    REFERENCES `mydb`.`Food` (`id_Food`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Guest_has_food_Guest1`
    FOREIGN KEY (`Id_guest`)
    REFERENCES `mydb`.`Guest` (`Id_guest`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Services`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Services` (
  `Id_service` INT NOT NULL,
  `Service` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`Id_service`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Payment`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Payment` (
  `Id_payment` INT NOT NULL,
  `Payment_type` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`Id_payment`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Guest_pay`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Guest_pay` (
  `Id_guest` INT NOT NULL,
  `Id_payment` INT NOT NULL,
  `Note` VARCHAR(45) NULL,
  INDEX `Id_guest_idx` (`Id_guest` ASC) VISIBLE,
  INDEX `Id_payment_idx` (`Id_payment` ASC) VISIBLE,
  CONSTRAINT `Id_guest`
    FOREIGN KEY (`Id_guest`)
    REFERENCES `mydb`.`Guest` (`Id_guest`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `Id_payment`
    FOREIGN KEY (`Id_payment`)
    REFERENCES `mydb`.`Payment` (`Id_payment`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Room`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Room` (
  `Id_room` INT NOT NULL,
  `Room_number` VARCHAR(45) NOT NULL,
  `Room_type` VARCHAR(45) NOT NULL,
  `Bed_number` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`Id_room`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Guest_has_room`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Guest_has_room` (
  `Id_guest` INT NOT NULL,
  `Id_room` INT NOT NULL,
  `Number_of_guests` INT NOT NULL,
  INDEX `Id_guest_idx` (`Id_guest` ASC) VISIBLE,
  INDEX `Id_room_idx` (`Id_room` ASC) VISIBLE,
  PRIMARY KEY (`Id_guest`, `Id_room`),
  CONSTRAINT `Id_guest`
    FOREIGN KEY (`Id_guest`)
    REFERENCES `mydb`.`Guest` (`Id_guest`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `Id_room`
    FOREIGN KEY (`Id_room`)
    REFERENCES `mydb`.`Room` (`Id_room`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Guest_has_Services`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Guest_has_Services` (
  `Id_guest` INT NOT NULL,
  `Id_service` INT NOT NULL,
  `Price` INT NOT NULL,
  INDEX `fk_Guest_has_Services_Services1_idx` (`Id_service` ASC) VISIBLE,
  INDEX `fk_Guest_has_Services_Guest1_idx` (`Id_guest` ASC) VISIBLE,
  PRIMARY KEY (`Id_guest`, `Id_service`),
  CONSTRAINT `fk_Guest_has_Services_Guest1`
    FOREIGN KEY (`Id_guest`)
    REFERENCES `mydb`.`Guest` (`Id_guest`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Guest_has_Services_Services1`
    FOREIGN KEY (`Id_service`)
    REFERENCES `mydb`.`Services` (`Id_service`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Contact`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Contact` (
  `Id_contact` INT NOT NULL,
  `Contact_mail` VARCHAR(45) NOT NULL,
  `Contact_phone` BIGINT(16) NOT NULL,
  `Id_guest` INT NOT NULL,
  PRIMARY KEY (`Id_contact`, `Id_guest`),
  INDEX `fk_Contact_Guest1_idx` (`Id_guest` ASC) VISIBLE,
  CONSTRAINT `fk_Contact_Guest1`
    FOREIGN KEY (`Id_guest`)
    REFERENCES `mydb`.`Guest` (`Id_guest`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Stay`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Stay` (
  `Id_stay` INT NOT NULL,
  `Start_date` DATE NOT NULL,
  `End_date` DATE NOT NULL,
  `Id_guest` INT NOT NULL,
  PRIMARY KEY (`Id_stay`, `Id_guest`),
  INDEX `fk_Stay_Guest1_idx` (`Id_guest` ASC) VISIBLE,
  CONSTRAINT `fk_Stay_Guest1`
    FOREIGN KEY (`Id_guest`)
    REFERENCES `mydb`.`Guest` (`Id_guest`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Rating`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Rating` (
  `Id_rating` INT NOT NULL,
  `Rating_value` INT NOT NULL,
  `Rating_comment` VARCHAR(45) NULL,
  `Id_guest` INT NOT NULL,
  PRIMARY KEY (`Id_rating`),
  INDEX `fk_Rating_Guest1_idx` (`Id_guest` ASC) VISIBLE,
  CONSTRAINT `fk_Rating_Guest1`
    FOREIGN KEY (`Id_guest`)
    REFERENCES `mydb`.`Guest` (`Id_guest`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


