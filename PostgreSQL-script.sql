-- -----------------------------------------------------
-- Table Guest
-- -----------------------------------------------------
CREATE TABLE Guest (
	Id_guest SERIAL,
	Name VARCHAR(45),
	Surname VARCHAR(45),
	Birth DATE,
	PRIMARY KEY (Id_guest)
);
-- -----------------------------------------------------
-- Table Stay
-- -----------------------------------------------------
CREATE TABLE Stay (
	Id_stay SERIAL,
	Start_date DATE,
	End_date DATE,
	Id_guest INT,
	PRIMARY KEY (Id_stay),
	FOREIGN KEY(Id_guest) REFERENCES Guest(Id_guest)
);

-- -----------------------------------------------------
-- Table Rating
-- -----------------------------------------------------
CREATE TABLE Rating (
	Id_rating SERIAL,
	Rating_value INT,
	Rating_comment VARCHAR(255),
	Id_guest INT,
	PRIMARY KEY (Id_rating),
	FOREIGN KEY(Id_guest) REFERENCES Guest(Id_guest)
);

-- -----------------------------------------------------
-- Table Contact
-- -----------------------------------------------------
CREATE TABLE Contact (
	Id_contact SERIAL,
	Contact_mail VARCHAR(45),
	Contact_phone INT,
	Id_guest INT,
	PRIMARY KEY (Id_contact),
	FOREIGN KEY(Id_guest) REFERENCES Guest(Id_guest)
);

-- -----------------------------------------------------
-- Table Room
-- -----------------------------------------------------
CREATE TABLE Room (
	Id_room SERIAL,
	Room_number VARCHAR(45),
	Room_type VARCHAR(45),
	Bed_number VARCHAR(45),
	PRIMARY KEY (Id_room)
);

-- -----------------------------------------------------
-- Table Payment
-- -----------------------------------------------------
CREATE TABLE Payment (
	Id_payment SERIAL,
	Payment_type VARCHAR(45),
	PRIMARY KEY (Id_payment)
);

-- -----------------------------------------------------
-- Table Service
-- -----------------------------------------------------
CREATE TABLE Services (
	Id_service SERIAL,
	Service VARCHAR(45),
	PRIMARY KEY (Id_service)
);
-- -----------------------------------------------------
-- Table Food
-- -----------------------------------------------------
CREATE TABLE Food (
	Id_Food SERIAL,
	Food_type VARCHAR(45),
	Food_price VARCHAR(45),
	PRIMARY KEY (Id_Food)
);
-- -----------------------------------------------------
-- Table Guest_has_room
-- -----------------------------------------------------
CREATE TABLE Guest_has_room (
	Id_guest INT,
	Id_room INT,
	Number_of_guests INT,
	FOREIGN KEY(Id_guest) REFERENCES Guest(Id_guest),
	FOREIGN KEY(Id_room) REFERENCES Room(Id_room)
);

-- -----------------------------------------------------
-- Table Guest_pay
-- -----------------------------------------------------
CREATE TABLE Guest_pay (
	Id_guest INT,
	Id_payment INT,
	Note VARCHAR(45),
	FOREIGN KEY(Id_guest) REFERENCES Guest(Id_guest),
	FOREIGN KEY(Id_payment) REFERENCES Payment(Id_payment)
);
-- -----------------------------------------------------
-- Table Guest_has_Services
-- -----------------------------------------------------
CREATE TABLE Guest_has_Services (
	Id_guest INT,
	Id_service INT,
	Price INT,
	FOREIGN KEY(Id_guest) REFERENCES Guest(Id_guest),
	FOREIGN KEY(Id_service) REFERENCES Services(Id_service)
);
-- -----------------------------------------------------
-- Table Guest_has_food
-- -----------------------------------------------------
CREATE TABLE Guest_has_food (
	Id_guest INT,
	Id_Food INT,
	Total_price INT,
	FOREIGN KEY(Id_guest) REFERENCES Guest(Id_guest),
	FOREIGN KEY(Id_Food) REFERENCES Food(Id_Food)
);

