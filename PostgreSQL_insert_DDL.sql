-- -----------------------------------------------------
-- Insert into Table Rating
-- -----------------------------------------------------
insert into rating (id_rating, rating_value, rating_comment, id_guest) values ('1', '10', 'spokojenost', '5');
insert into rating (id_rating, rating_value, rating_comment, id_guest) values ('2', '8', 'bylo fajn jen koupelny maly', '25');
insert into rating (id_rating, rating_value, rating_comment, id_guest) values ('3', '9', 'parecky vytecny, koupelny maly, manzel upad jinak dobry', '2');
insert into rating (id_rating, rating_value, rating_comment, id_guest) values ('4', '4', 'jsem nenasel pokoj', '12');
insert into rating (id_rating, rating_value, rating_comment, id_guest) values ('5', '10', 'dobrzej ale nikto polak', '15');
insert into rating (id_rating, rating_value, rating_comment, id_guest) values ('6', '7', 'velnes fajny, jidlo nic moc', '21');

-- -----------------------------------------------------
-- Insert into Table Guest_has_service
-- -----------------------------------------------------
insert into guest_has_services (Id_guest, Id_service, Price) values ('1', '1', '400');
insert into guest_has_services (Id_guest, Id_service, Price) values ('2', '2', '290');
insert into guest_has_services (Id_guest, Id_service, Price) values ('3', '3', '200');
insert into guest_has_services (Id_guest, Id_service, Price) values ('4', '4', '250');
insert into guest_has_services (Id_guest, Id_service, Price) values ('5', '5', '340');
insert into guest_has_services (Id_guest, Id_service, Price) values ('6', '4', '250');
insert into guest_has_services (Id_guest, Id_service, Price) values ('7', '3', '200');
insert into guest_has_services (Id_guest, Id_service, Price) values ('8', '2', '290');
insert into guest_has_services (Id_guest, Id_service, Price) values ('9', '1', '400');
insert into guest_has_services (Id_guest, Id_service, Price) values ('10', '4', '250');




